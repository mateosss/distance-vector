cplusplus {{
#define DATA_PACKET 0
#define HELLO_PACKET 1
#define HELLO_ACK_PACKET 2
#define TABLE_PACKET 3
#define TABLE_REQUEST_PACKET 4
}}

packet DataPacket {
    name = "data";
    kind = DATA_PACKET;
    int source;
    int destination;
    int hopCount;
}

// Packet used for signaling a host presence and to 
// measure hop cost to each neighbor
packet HelloPacket {
    name = "hello";
    kind = HELLO_PACKET;
    int source;
}

// Answer to a HelloPacket, lets measure hop cost to
// new neighbor, and update its routing table
packet HelloAckPacket {
    name = "helloack";
    kind = HELLO_ACK_PACKET;
    int source;
}

// A row in TablePacket
struct TablePacketRow {
    int host;
    int cost;
}

// Packet that synthesizes a host routing table
// and makes its neighbors update theirs
packet TablePacket {
    name = "table";
    kind = TABLE_PACKET;
    int source;
    TablePacketRow table[];
}

packet TableRequestPacket {
	name = "tablerequest";
	kind = TABLE_REQUEST_PACKET;
}
