# Enrutamiento Vector Distancia Estático

Implementación de un algoritmo similar a Vector-Distancia (RIP, BGP, etc)
sencillo para redes estáticas en principio, pero con posibilidad de ser
exténdido para redes dinámicas.

# Informe

El informe se encuentra en informe/informe.md y también hay una versión en pdf.

# Clases

- `App`: Aplicación que emite/recibe `DataPacket`s
- `Net`: Capa de red que decide por donde redirigir cada DataPacket
- `Lnk`: Capa de enlace, representa el buffer de cada línea de salida de `Net`

# Clases de Paquetes del Protocolo

- `DataPacket`: Paquete de datos, principal paquete a transferir
- `HelloPacket`: Se utiliza para anunciarse a todos los vecinos de un `Net`
- `HelloAckPacket`: Permite calcular el costo de la conexión, y si la hay
- `TableRequestPacket`: Solicita la tabla a un vecino
- `TablePacket`: Resume la tabla de enrutamiento y propaga los cambios en la red
