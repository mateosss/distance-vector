simple App
{
    parameters:
        volatile double interArrivalTime = default(0); // sec
        int packetByteSize = default(125); // bytes
        int destination = default(0);
        @display("i=block/app");
    gates:
        inout portNet;
}

simple Net
{
    parameters:
        @display("i=block/dispatch");
    gates:
        inout portApp;
        inout portLnk[];
}

simple Lnk
{
    parameters:
        @display("i=block/queue");
    gates:
        inout portNet;
        inout portOut;
}

module Node
{
    parameters:
        @display("i=block/routing");
        int interfaces;
    gates:
        inout portNod[interfaces];
    submodules:
        app: App {
            parameters:
                @display("p=75,50");
        }
        net: Net {
            parameters:
                @display("p=75,120");
        }
        lnk[interfaces]: Lnk {
            parameters:
                @display("p=75,190,r,70");
        }
    connections allowunconnected:
        app.portNet <--> net.portApp;
        for i=0..interfaces-1 {
            net.portLnk++ <--> lnk[i].portNet;
            lnk[i].portOut <--> portNod[i];
        }
}

network Network
{
    submodules:
        node[8]: Node {
            @display("p=100,100,ri,100,100");
            interfaces=2;
        }
    connections allowunconnected:
        node[0].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[1].portNod[0];
        node[1].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[2].portNod[0];
        node[2].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[3].portNod[0];
        node[3].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[4].portNod[0];
        node[4].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[5].portNod[0];
        node[5].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[6].portNod[0];
        node[6].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[7].portNod[0];
        node[7].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[0].portNod[0];
}

network NetworkStar
{
    submodules:
        node[57]: Node {
            @display("i=misc/node_vs,gold");
            interfaces=4;
        }
    connections allowunconnected:
        node[0].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[1].portNod[2];
        node[1].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[2].portNod[2];
        node[1].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[4].portNod[2];
        node[3].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[4].portNod[3];
        node[4].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[5].portNod[2];
        node[4].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[7].portNod[2];
        node[5].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[6].portNod[2];
        node[5].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[10].portNod[2];
        node[6].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[7].portNod[3];
        node[6].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[9].portNod[2];
        node[7].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[8].portNod[2];
        node[7].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[12].portNod[2];
        node[9].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[11].portNod[2];
        node[10].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[11].portNod[3];
        node[10].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[13].portNod[2];
        node[11].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[12].portNod[3];
        node[11].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[14].portNod[2];
        node[12].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[15].portNod[2];
        node[13].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[18].portNod[2];
        node[14].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[15].portNod[3];
        node[14].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[22].portNod[2];
        node[15].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[16].portNod[2];
        node[16].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[17].portNod[2];
        node[16].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[23].portNod[2];
        node[18].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[19].portNod[2];
        node[18].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[21].portNod[2];
        node[19].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[20].portNod[2];
        node[19].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[26].portNod[2];
        node[20].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[21].portNod[3];
        node[20].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[24].portNod[2];
        node[21].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[22].portNod[3];
        node[21].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[23].portNod[3];
        node[23].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[24].portNod[3];
        node[24].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[28].portNod[2];
        node[25].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[27].portNod[2];
        node[26].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[32].portNod[2];
        node[27].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[29].portNod[2];
        node[27].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[30].portNod[2];
        node[28].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[29].portNod[3];
        node[28].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[31].portNod[2];
        node[29].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[31].portNod[3];
        node[30].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[36].portNod[2];
        node[31].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[33].portNod[2];
        node[32].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[34].portNod[2];
        node[33].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[37].portNod[2];
        node[34].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[35].portNod[2];
        node[34].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[40].portNod[2];
        node[35].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[36].portNod[3];
        node[35].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[39].portNod[2];
        node[36].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[37].portNod[3];
        node[37].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[38].portNod[2];
        node[38].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[39].portNod[3];
        node[38].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[42].portNod[2];
        node[39].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[40].portNod[3];
        node[39].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[41].portNod[2];
        node[40].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[43].portNod[2];
        node[40].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[44].portNod[3];
        node[41].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[42].portNod[3];
        node[41].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[43].portNod[3];
        node[43].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[46].portNod[2];
        node[44].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[45].portNod[2];
        node[44].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[47].portNod[2];
        node[45].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[46].portNod[3];
        node[46].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[49].portNod[2];
        node[47].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[48].portNod[2];
        node[48].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[50].portNod[2];
        node[48].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[51].portNod[2];
        node[49].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[50].portNod[3];
        node[50].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[53].portNod[2];
        node[51].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[52].portNod[2];
        node[51].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[53].portNod[3];
        node[52].portNod[1] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[54].portNod[2];
        node[53].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[56].portNod[2];
        node[54].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[55].portNod[2];
        node[55].portNod[0] <--> {  datarate = 1Mbps; delay = 100us; } <--> node[56].portNod[3];
}


