#ifndef APP
#define APP

#include <string.h>
#include <omnetpp.h>
#include <packet_m.h>

using namespace omnetpp;

class App: public cSimpleModule {
private:
    cMessage* sendMsgEvent;
    cOutVector recvDelay;
    cOutVector sentDelay;
    cStdDev hopStats;
public:
    App();
    virtual ~App();
protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage* msg);

    virtual void collectHops(DataPacket* data);
    virtual void recordTransmittedDelay(DataPacket* data, simtime_t delay);
};

Define_Module(App);

#endif /* APP */

App::App() {
}

App::~App() {
}

void App::initialize() {

    // If interArrivalTime for this node is higher than 0
    // initialize packet generator by scheduling sendMsgEvent
    if (par("interArrivalTime").doubleValue() != 0) {
        sendMsgEvent = new cMessage("sendEvent");
        scheduleAt(par("interArrivalTime"), sendMsgEvent);
    }

    // Initialize statistics
    recvDelay.setName("ReceivedDelay");
    sentDelay.setName("SentDelay");
    hopStats.setName("TotalHops");
}

void App::finish() {
    // Record statistics
    recordScalar("Number of packets received", recvDelay.getValuesReceived());
    recordScalar("Number of packets sent that arrived", sentDelay.getValuesReceived());
    recordScalar("Average hops", hopStats.getMean());
}

void App::handleMessage(cMessage* msg) {

    // if msg is a sendMsgEvent, create and send new packet
    if (msg == sendMsgEvent) {
        // create new packet
        DataPacket* data = new DataPacket();
        data->setByteLength(par("packetByteSize"));
        data->setSource(this->getParentModule()->getIndex());
        data->setDestination(par("destination"));

        // send to net layer
        send(data, "portNet$o");

        // compute the new departure time and schedule next sendMsgEvent
        simtime_t departureTime = simTime() + par("interArrivalTime");
        scheduleAt(departureTime, sendMsgEvent);

    }
    // else, msg is a packet from net layer
    else {
        // compute delay and record statistics
        DataPacket* data = static_cast<DataPacket*>(msg);
        simtime_t delay = simTime() - msg->getCreationTime();
        recvDelay.record(delay);
        recordTransmittedDelay(data, delay);
        collectHops(data);
        delete msg;
    }

}

void App::collectHops(DataPacket* data) {
    static_cast<App*>(
        getParentModule()->getParentModule() // Network
        ->getSubmodule("node", data->getSource()) // Source node
        ->getSubmodule("app") // Source App
    )->hopStats.collect(data->getHopCount());
}

void App::recordTransmittedDelay(DataPacket* data, simtime_t delay) {
    static_cast<App*>(
        getParentModule()->getParentModule() // Network
        ->getSubmodule("node", data->getSource()) // Source node
        ->getSubmodule("app") // Source App
    )->sentDelay.record(delay);
}
