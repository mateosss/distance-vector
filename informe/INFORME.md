# Enrutamiento Vector Distancia Estático

***Algoritmo distribuido de capa de red que encuentra la línea más eficiente
por la que enrutar un paquete en una red estática, sin considerar congestión***

## Introducción

Al estudiar la capa de red, el principal problema a resolver es el de
enrutamiento de paquetes. ¿Cómo determina un enrutador por qué línea enviar un
paquete recibido? El problema que se nos presenta es uno de sincronización y
optimización de flujo en un panorama distribuido.

Pasaremos de una heurística sencilla, a un algoritmo un poco más complejo que
intenta resolver el problema de cómo propagar la información de la topología de
la red a todos los enrutadores que la necesiten y cómo estos determinarán la
línea de salida que dará el menor tiempo de viaje para un paquete dado.

## Topología de Anillo

Se presenta la siguiente topología de red a estudiar, donde cada nodo representa
una entidad de red conectada a una aplicación.

![](img/model.png)

*Figura: A la izquierda: Topología de la red. A la derecha: Composición de cada nodo en
la red, 2 buffers de capa de enlace, una entidad de capa de red, y una de
aplicación*

Llamaremos `node` a un host de la red, `net` a su entidad de red, `lnk[i]` a su
entidad de enlace `i` *(o línea `i` de salida/entrada)*, y `app` a la aplicación
que recibe o emite los paquetes.

### Heurística Inicial

Dada esta estructura cíclica, es sencillo ver que si tomamos la siguiente medida
al recibir un paquete en la entidad de red, el paquete llegará eventualmente a
destino:

  - Si el paquete tiene como destino este `node`, enviar paquete hacia `app`  
  - Sino retransmitirlo por `lnk[0]`  

Y es precisamente esta heurística la que utilizaremos como base de comparación
con nuestro algoritmo.

### Algoritmo Implementado

Al pensar en un algoritmo, no quisimos tener la complejidad de tener que
replicar un grafo entero en cada `net`, es por esto que preferimos el enfoque
de algo como `RIP` que simplemente mantienen una tabla de host destino,
línea de salida conveniente y costo a ese destino:

```cpp
struct row_t { int host, line, cost; };
vector<row_t> table;
```

Ahora lo que se debe hacer es que cada enrutador calcule el costo hacia
sus vecinos y les solicite a estos sus respectivas tablas. Luego de cada
actualización de la tabla es posible propagar la información a cada vecino, y
estos a su vez, si su tabla cambiase, propagaran la suya a sus vecinos, y así
sucesivamente.

El algoritmo implementa paquetes propios `HelloPacket`, `HelloAckPacket`,
`TableRequestPacket`, y `TablePacket`, todos estos al llegar a un `lnk` son
encolados con máxima prioridad, para así poder actualizar los datos de la red
rápidamente antes de continuar enviando datos.

Las tres principales funcionalidades que ejecuta el algoritmo son las
siguientes:

- **Anunciarse y obtener una tabla inicial:** Se utilizará un `HelloPacket` que
  será enviado por cada `lnk`, este será contestado con un `HelloAckPacket` por
  la misma línea con la identificación del host vecino, permitiéndole al host
  anunciante, calcular el tiempo de ida y vuelta que hay por esa línea hacia su
  vecino. Y luego se pedirá al vecino su tabla con un `TableRequestPacket`. Con
  esto ya se tiene toda la información necesaria de ese vecino en nuestra tabla.
  Notar además que si no existiese un host vecino por esa línea, el
  `HelloPacket` nunca sería contestado, haciendo que efectivamente, nunca se
  agregue una entrada a la tabla que utilice la línea desconectada.

- **Propagar la tabla:** Ante cada actualización en la tabla, un host procederá
  a enviar en difusión `TablePacket` a todos sus vecinos *(exceptuando el que
  hizo que la tabla fuera actualizada, ya que este ya posee la información que
  será propagada)*. De esta forma cualquier cambio importante generará cambios
  en los correspondientes vecinos de forma sucesiva, y así estos se verán
  efectivamente propagados por la totalidad de la red.

- **Actualizar la tabla:** Una actualización en la tabla puede darse
  inicialmente al reconocer a los vecinos propios o al recibir un `TablePacket`.
  En cualquier caso el algoritmo de actualización determina por cada entrada en
  la tabla recibida, si no se sabe como llegar a alguno de los hosts en el
  `TablePacket` o si alguno de los costos (sumado al costo en ir al host que me
  envió la tabla) es menor al costo que se tiene actualmente registrado en la
  tabla propia. En cualquiera de los casos anteriores, la tabla será actualizada
  y propagada.

Por último cabe aclarar que el único supuesto que tuvimos en el algoritmo es que
la red es estática, para soportar redes dinámicas ver la sección de *Trabajos
Futuros*. Si bien no es 100% necesario, una capa de enlace que detecte paquetes
perdidos, ayudaría mucho a la eficiencia.

## Definición de los casos de estudio

Definiremos dos casos de estudio en nuestra red en forma de anillo:

- `Caso 1`: en este `node[0]` y `node[2]` serán los encargados de generar cada
  segundo aproximadamente *(distribución exponencial centrada en 1 segundo)*
  paquetes con destino `node[5]`.

- `Caso 2`: en este caso, todos los nodos *(`node[0..7]`)* generarán paquetes
  con destino `node[5]`, el intervalo de generación de paquetes será, en
  principio, el mismo, pero estudiaremos también, a partir de cuál intervalo la
  congestión comienza a cesar, y veremos cómo con el algoritmo, esto sucede
  antes.

*En ambos casos, lo que se transmitirán serán paquetes de **125000 bytes***

## Resultados

A continuación compararemos los resultados obtenidos por la heurística, con
respecto al algoritmo, en los distintos casos de la simulación.

### Caso 1

En este caso, y con la heurística inicial, como los paquetes siempre se
transmiten por la línea de salida `lnk[0]`, el tramo que incluye a los
`node[3,4,5]` es desperdiciado. Por otro lado `node[0]`, al generar y recibir
paquetes de origen `node[2]`, es el nodo más afectado por la congestión. Este
comportamiento también provoca que el delay de los paquetes recibidos en
`app[5]` aumente.

Por su parte, con la implementación del algoritmo, los nodos poseen una tabla
con la que pueden elegir la línea de salida que les garantice el menor tiempo de
llegada a destino. Entonces el `node[2]` elige la salida `lnk[1]` lo que hace
que el flujo que antes era causante de congestión en `node[0]`, sea redirigido,
los buffers no se congestionen, y todo esto resulta en que una mayor cantidad de
paquetes llegan a destino en comparación a la heurística inicial.

Analizaremos a continuación algunas métricas:

![](img/case1_buffer.png)

*Figura - Buffer, Caso 1: Aquí podemos observar el comportamiento del buffer
lnk[0] de node[0] con y sin algoritmo. Utilizamos únicamente este buffer ya que
los demás no presentan saturación en ninguno de los dos casos*

![](img/case1_delay.png)

*Figura - Delay, Caso 1: Por la misma saturación que presenta la figura
anterior, el delay de los paquetes crece linealmente, a diferencia de el caso
con algoritmo, que se mantiene más o menos constante*

| Caso 1 (390)  | Paquetes Recibidos | Recibidos / Generados   |
| :------------ | :----------------- | :---------------------- |
| Sin Algoritmo | 196                | 50.25%                  |
| Con Algoritmo | 379                | 97.17%                  |

*Paquetes recibidos del nodo destino `node[5]`*

Además en el caso con algoritmo, el camino más largo usa 4 hops, a diferencia
del caso sin algoritmo que usa 5.

### Caso 2

En el caso 2, con la heurística inicial, encontramos que, salvo el `node[5]`
*(que es el destino)* y el `node[4]` *(que es el único por el cual no pasa
ninguna ruta de envío)*, todos los nodos, se encuentran en distintos niveles de
saturación, de mayor a menor según que si se encuentran más o menos lejos del
`node[5]` en el sentido correspondiente. Por culpa de esto el delay de los
paquetes recibidos crece linealmente.

Por otro lado, al implementar el algoritmo, en principio, la congestión sigue
estando, ya que la red realmente está recibiendo más carga de la que puede
soportar, sin embargo veremos que la cantidad de paquetes recibidos efectivos
es mucho mayor con el algoritmo, y de esta forma, se ve que el algoritmo
aprovecha de mejor forma los recursos. Como nota, podemos notar que el uso
del buffer de `lnk[1]` aparece siendo utilizado en este caso.

A diferencia del caso 1, en este no hay métricas comparables de tamaño de buffer
ni de delay que podamos mostrar en gráficos ya que la realidad es que en ambos
casos los delays y buffers crecen, por que la red está siendo sobre exigida,
sin embargo ver los resultados de cuántos de los paquetes generados llegan
efectivamente resulta una buena métrica para comparar.

| Caso 2 (1378) | Paquetes Recibidos | Recibidos / Generados   |
| :------------ | :----------------- | :---------------------- |
| Sin Algoritmo | 199                | 14.44%                  |
| Con Algoritmo | 398                | 28.89%                  |

*Es lógico ver que el goodput se duplique ya que se utilice una ruta extra con
la  misma capacidad que en el caso sin algoritmo.*

![](img/case2_hops.png)

*Figura - Hops, Caso 2: Distancia en hops de cada nodo hacia el destino, notar que el
camino promedio en el caso sin algoritmo es **3.5 hops** y con algoritmo es de
**2 hops***

Otra estadística a notar es en cuanto sufren los nodos más distantes al destino.
En el caso sin algoritmo, el host más lejano es el `node[4]`, del cual en los
200 segundos de la simulación, solo logra hacer llegar **2 paquetes** al
`node[5]`. Con algoritmo por otro lado, el nodo más afectado es el `node[1]` del
cual llegan a destino **19 paquetes**

#### Variando el Intervalo de Generación

Además en este caso exploramos a partir de que valor de la variable
`intervalArrivalTime` que dicta el intervalo de generación de paquetes de cada
nodo, la red comienza a ganar estabilidad. Con estabilidad nos referimos a, en
que momento, los buffers dejan de crecer indefinidamente y se mantienen en un
valor constante *(particularmente entre 0 y 1)*. Para este caso quitamos la
distribución aleatoria de la variable y reemplazamos por una constante para
estudiar los efectos considerando que la simulación tiende a duración infinita.

Haciendo una cuenta sencilla vemos con los siguientes datos que:

- Tamaño de paquete $p = 12500 Bytes = 1Mb$

- Datarate de un hop $r = 1Mbps$

- Tiempo de procesamiento del primer paquete por nodo $x = 0 secs$ *(esto viene
  dado)*

$\Rightarrow$ un paquete recorre un hop en $\frac{p}{r} = \frac{1Mb}{1Mb/s} = 1
sec.$ y su paso por un nodo es instantáneo.

$\Rightarrow$ con $N$ el mayor número de hops que tiene que recorrer un
paquete en la red, se necesitan $Nsegundos$ para que la red se mantenga estable.
Ya que si no, habría todavía paquetes en buffer cuando todos los nodos comiencen
a transmitir de vuelta.

Conociendo esto, y sabiendo el número máximo de hops que utiliza cada instancia
podemos dar el interArrivalTime que mantiene estable la red:

- Sin algoritmo: El host más lejano a `node[5]` es `node[4]` con **7 hops** de
  distancia

- Con algoritmo: Al distribuirse en el camino más cercano, el host más lejano
  es aquel que queda opuesto al destino: `node[1]` con **4 hops**

Entonces con algoritmo, vemos que se pueden generar datos mucho más rápido
*(cada **4 segundos** contra **7 segundos** sin algoritmo)* y la red va a
mantenerse estable.

## Otras Configuraciones

A continuación mostraremos otras configuraciones de red que muestran algunos
comportamientos deseables del algoritmo en acción.

### Detección de los Costos

Supongamos la misma network en forma de anillo pero con la diferencia de que las
conexiones que pasan por los `node[0,7,6,5]` tienen un datarate muy lento.
Supongamos ahora que queremos enviar de `node[0]` a `node[5]`.

Por cómo se calcula el costo a un vecino *(basándose en el tiempo de transmisión
de un `HelloPacket`)*, se están teniendo en cuenta de forma implícita, aspectos
físicos como la velocidad de transmisión y el retardo de propagación del medio
en cada conexión entre dos enrutadores

Es por esto que si bien el camino `0,1,2,3,4,5` es más *largo* en hops,
es este el que se elegirá ya que es el más *rápido*.

### Topologías más Complejas

Otras bondades del algoritmo es que escala fácilmente a redes más variadas, sin
restricciones de cantidad de nodos, ni de líneas, y no solo eso, si no que la
propagación de las tablas (al menos la inicial) en la siguiente topología con 57
nodos y hasta 4 líneas de salida por nodo, es relativamente rápida: **3.124
ms**.

Esta propagación es veloz en gran parte por la prioridad que tienen los paquetes
de red por sobre los de datos.

![](img/star.png)

*Figura - Topología Estrella: `node[{0, 3, 17, 21, 25}]` enviando a `node[55]`*

## Trabajo Futuro

Hay varias características que no se llegaron a realizar, que no deberían
presentar una mayor complicación ya que los elementos para realizarlas están
actualmente implementados.

### Detección de Nodos Nuevos

Dado el caso de que se sume un nuevo nodo a la red, este se anunciaría y el
protocolo debería funcionar de forma prevista, propagando la actualización
de las tablas en poco tiempo.

Sin embargo se encuentran algunos casos borde que hacen que, en nuestra
implementación (branch `catch-hostup`), que implementa una clase `UpNet` que es
un nodo que se habilita en el segundo **100** de la simulación, no esté
funcionando.

### Detección de Nodos Caídos

Para detectar la caída imprevista de un nodo, deberíamos implementar algún
mecanismo de `polling` entre vecinos, cada nodo preguntaría periódicamente a
todos sus vecinos si siguen vivos en la red. De no recibir respuesta una cierta
cantidad de veces, se determinaría que el host se ha caído, y cualquier entrada
de la tabla de enrutamiento que saliese por esa línea debería ser eliminada, a
su vez, se debería informar de la caída del host a todos los demás vecinos. Con
esta información, estos deberían propagar la información de la caída,
y luego de esto, las tablas deberían ser rearmadas, de forma habitual.

### Detección de Congestión

La principal omisión del algoritmo, es que ignora completamente la congestión,
es decir, que si todos los hosts deciden que el mejor camino es siempre uno,
entonces ningún otro camino será utilizado y este se congestionará.

Supongamos ahora que cambiamos el tipo de la tabla a

```cpp
struct row_t {
  int host;
  linked_list<int, int> line_cost;
};
vector<row_t> table;
```

En donde `line_cost` es una lista enlazada y ordenada de menor a mayor costo
de todas las posibles líneas de salidas utilizables para llegar a un destino.

Ahora, volviendo al problema de la congestión, una forma de solucionar esto,
sería la implementación de un `threshold` en cada `Net` que determine si un host
`source` determinado está causando saturación y de ser así, emitir un paquete
`SlowDown` con ese nodo como destino, que será enrutado normalmente por la red.
Al llegar este paquete, el nodo causante de la congestión debería, dejar de
utilizar el índice 0 de su lista enlazada en la fila que está utilizando para el
host que le envió el `SlowDown`, comenzar a utilizar la segunda opción, y
retransmitir su actualización de la tabla, con la esperanza de que saliendo por
ahí, se evite la congestión.

De esta forma siempre que haya problemas de congestión, un nodo se enterará, y
utilizará una segunda opción que quizás la evite, y de no ser así, se continuará
con el proceso hasta que eventualmente suceda.

### Detección de loops

Implementando las mejoras discutidas en esta sección, el algoritmo se vería
libre de loops de enrutamiento, ya que al producirse cualquier caída de un host,
esta se ve propagada a *todos* los nodos de la red, con mayor o menor rapidez,
pero eventualmente ocurre, y al ocurrir, el loop se rompe.

# Referencias

- *A. S. Tanembaum - Computer Networks 5th Edition*
- *Kurose, J. F. and Ross, K. W. Computer Networking - A Top Down Approach. 7th
  Edition*
- *[Distance Vector Routing Protocol - Wikipedia EN](https://en.wikipedia.org/wiki/Distance-vector_routing_protocol)*
- *[Link State Routing Protocol - Wikipedia EN](https://en.wikipedia.org/wiki/Link-state_routing_protocol)*
- *[Routing Protocol Comparisson - geeksforgeeks.org](https://www.geeksforgeeks.org/computer-network-distance-vector-routing-vs-link-state-routing/)*
