#ifndef LNK
#define LNK

#include <string.h>
#include <omnetpp.h>
#include <packet_m.h>

using namespace omnetpp;

class Lnk: public cSimpleModule {
private:
    cQueue buffer;
    cMessage *endServiceEvent;
    simtime_t serviceTime;
    cOutVector bufferSizeVector;
public:
    Lnk();
    virtual ~Lnk();
protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);
};

Define_Module(Lnk);

#endif /* LNK */

Lnk::Lnk() {
    endServiceEvent = NULL;
}

Lnk::~Lnk() {
    cancelAndDelete(endServiceEvent);
}

void Lnk::initialize() {
    endServiceEvent = new cMessage("endService");
    bufferSizeVector.setName("Buffer Size");
}

void Lnk::finish() {
}

void Lnk::handleMessage(cMessage *msg) {
    // If this lnk is not connected, drop packet
    if (!gate("portOut$o")->isPathOK()) {
        delete msg;
        return;
    }

    if (msg == endServiceEvent) {
        if (!buffer.isEmpty()) {
            cPacket* pkt = static_cast<cPacket*>(buffer.pop());
            bufferSizeVector.record(buffer.getLength());
            send(pkt, "portOut$o");
            serviceTime = pkt->getDuration();
            scheduleAt(simTime() + serviceTime, endServiceEvent);
        }
    } else { // msg is a packet
        if (msg->arrivedOn("portNet$i")) { // msg came from upper layer
            // Enqueue prioritizing non DATA_PACKETs (routing protocol packets)
            if (msg->getKind() == DATA_PACKET || buffer.isEmpty()) buffer.insert(msg);
            else buffer.insertAfter(buffer.front(), msg);
            bufferSizeVector.record(buffer.getLength());
            // Schedule if the server is idle
            if (!endServiceEvent->isScheduled()) scheduleAt(simTime() + 0, endServiceEvent);
        } else send(msg, "portNet$o");
    }
}
