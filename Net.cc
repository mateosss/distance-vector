#ifndef NET
#define NET

#include <string.h>
#include <omnetpp.h>
#include <packet_m.h>
#include <vector>
#include <iostream>

using namespace omnetpp;
using namespace std;

struct row_t { // Routing table rows
    int host;
    int line;
    int cost;
};

class Net: public cSimpleModule {
private:
    vector<row_t> table; // Routing table
    vector<int> gateHelloSentTime; // Used on broadcastHello(), in nanoseconds
public:
    Net();
    virtual ~Net();
protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage* msg);

    virtual void handleHelloPacket(cMessage* msg);
    virtual void handleHelloAckPacket(cMessage* msg);
    virtual void handleTablePacket(cMessage* msg);
    virtual void handleTableRequestPacket(cMessage* msg);
    virtual void handleDataPacket(cMessage* msg);

    virtual int getAddress();
    virtual void redirectData(DataPacket* data);
    virtual void broadcastHello();

    virtual row_t* getTableRow(int host);
    virtual TablePacket generateTablePacket();
    virtual void broadcastTable(int excluded);
    virtual void requestTable(int line);
    virtual bool updateTable(int host, int line, int cost);
    virtual void dumpTable();
};

Define_Module(Net);

#endif /* NET */

Net::Net() { }

Net::~Net() { }

void Net::initialize() {
    gateHelloSentTime.resize(gateSize("portLnk$o"));
    broadcastHello();
}

void Net::finish() {

}

void Net::handleMessage(cMessage* msg) {
    // All msg (events) on net are packets
    if (msg->getKind() == HELLO_PACKET) handleHelloPacket(msg);
    else if (msg->getKind() == HELLO_ACK_PACKET) handleHelloAckPacket(msg);
    else if (msg->getKind() == TABLE_PACKET) handleTablePacket(msg);
    else if (msg->getKind() == TABLE_REQUEST_PACKET) handleTableRequestPacket(msg);
    else if (msg->getKind() == DATA_PACKET) handleDataPacket(msg);
    else fprintf(stderr, "Incorrect packet received of kind %d", msg->getKind());
}

void Net::handleHelloPacket(cMessage* msg) {
    // Answers hello ack through the same line the hello came
    HelloPacket* hello = static_cast<HelloPacket*>(msg);
    HelloAckPacket* helloAck = new HelloAckPacket();
    helloAck->setSource(getAddress());
    send(helloAck, "portLnk$o", hello->getArrivalGate()->getIndex());
    delete hello;
}

void Net::handleHelloAckPacket(cMessage* msg) {
    // Calculates cost and adds proper row to table
    HelloAckPacket* helloAck = static_cast<HelloAckPacket*>(msg);
    int host = helloAck->getSource();
    int line = helloAck->getArrivalGate()->getIndex();
    int cost = simTime().inUnit(SIMTIME_US) - gateHelloSentTime[line];
    // if (getAddress() == 0) printf("[HelloAckPacket -> Host#%d] host = %d, line = %d, cost = %d\n", getAddress(), host, line, cost); fflush(stdout);
    updateTable(host, line, cost);
    requestTable(line);
    delete helloAck;
}

void Net::handleTablePacket(cMessage* msg) {
    // Update table with minimum (costs + line cost) in rows, and broadcastTable
    TablePacket* tablePacket = static_cast<TablePacket*>(msg);

    row_t* trow = getTableRow(tablePacket->getSource());
    if (!trow) { // Check if we know the source, if not we can't know the hop cost, and should drop the TablePacket
        delete tablePacket;
        return;
    }

    int hopCost = trow->cost;
    bool shouldUpdateTable = false;
    int line = tablePacket->getArrivalGate()->getIndex();
    for (int i = 0; i < tablePacket->getTableArraySize(); i++) {
        TablePacketRow row = tablePacket->getTable(i);
        shouldUpdateTable |= updateTable(row.host, line, row.cost + hopCost);
    }
    if (shouldUpdateTable) broadcastTable(line);
    delete tablePacket;
}

void Net::handleTableRequestPacket(cMessage* msg) {
    // Sends table packet through incoming msg line
    send(new TablePacket(generateTablePacket()), "portLnk$o", msg->getArrivalGate()->getIndex());
}

void Net::handleDataPacket(cMessage* msg) {
    // If destination is us, pass to app, else, redirect selecting best exit line through table
    DataPacket* data = static_cast<DataPacket*>(msg);
    if (data->getDestination() == getAddress()) send(msg, "portApp$o");
    else redirectData(data);
}


int Net::getAddress() {
    return this->getParentModule()->getIndex();
}

void Net::redirectData(DataPacket* data) {
    // Looks for the line which a packet should be sent through and sends it
    // If the data destination is not charged, drops the packet
    row_t* row = getTableRow(data->getDestination());
    if (row) {
        data->setHopCount(data->getHopCount() + 1);
        send(data, "portLnk$o", row->line);
    } else delete data;
}

void Net::broadcastHello() {
    // Send hello through all lines (to all neighbors)
    for (int i = 0; i < gateSize("portLnk$o"); i++) {
        HelloPacket* hello = new HelloPacket();
        hello->setSource(getAddress());
        hello->setByteLength(128);
        gateHelloSentTime[i] = simTime().inUnit(SIMTIME_US);
        send(hello, "portLnk$o", i);
    }
}

void Net::broadcastTable(int excluded) {
    // Broadcasts table to all neighbors except excluded line
    TablePacket ptable = generateTablePacket();
    for (int i = 0; i < gateSize("portLnk$o"); i++)
        if (i != excluded) send(new TablePacket(ptable), "portLnk$o", i);
}

void Net::requestTable(int line) {
    TableRequestPacket* tableReq = new TableRequestPacket();
    send(tableReq, "portLnk$o", line);
}

bool Net::updateTable(int host, int line, int cost) {
    // Tries to improve the routing table, returns wether it could
    if (host == getAddress()) return false; // This host should never appear in the table
    row_t* row = getTableRow(host);
    row_t newRow{host, line, cost};
    bool improved = true;

    // If the row exists, would it be improved? else add the row
    if (row) {
        if (cost < row->cost) *row = newRow;
        else improved = false;
    } else table.push_back(newRow);

    // if (getAddress() == 0) dumpTable();
    // printf("Host#%d - table.size = %d\n", getAddress(), table.size());fflush(stdout);
    return improved;
}

row_t* Net::getTableRow(int host) {
    row_t* row = nullptr;
    for (int i = 0; i < table.size() && !row; i++)
        if (table[i].host == host) row = &table[i];
    return row;
}

TablePacket Net::generateTablePacket() {
    // Synthesizes table to a TablePacket
    TablePacket ptable;
    ptable.setSource(getAddress());
    ptable.setTableArraySize(table.size());
    for (int i = 0; i < table.size(); i++) { // Fill packet table
        TablePacketRow row;
        row.host = table[i].host;
        row.cost = table[i].cost;
        ptable.setTable(i, row);
    }
    return ptable;
}

void Net::dumpTable(){
    printf("[%d Route Table]\n", getAddress());
    printf("Host\tLine\tCost\n");
    for (int i = 0; i < table.size(); i++)
        printf("%d\t%d\t%d\n", table[i].host, table[i].line, table[i].cost);
}
